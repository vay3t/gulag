FROM golang:1.17-alpine AS builder
RUN apk add --no-cache upx gcc musl-dev
WORKDIR /go/src/gulag
ENV GOOS=linux
ENV GOVERSION=1.19
ENV GO111MODULE=auto
ENV GOPATH=/go
ENV PATH=$GOPATH/bin:$PATH
COPY * .
RUN go install -ldflags "-s -w" -trimpath .
RUN upx --ultra-brute --lzma -9 --best /go/bin/gulag

FROM alpine:3.16.0
WORKDIR /usr/bin/
COPY --from=builder /go/bin/gulag .

ENTRYPOINT ["/usr/bin/gulag"]
